<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package TemplateOcean
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1, user-scalable=1"/>

<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri(); ?>/images/favicon-152.png">

<svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
<symbol id="icon-eye" viewbox="0 0 512 512">
<path d="m256 128c-82 0-146 49-224 128 67 68 124 128 224 128 100 0 173-76 224-127-52-58-125-129-224-129z m0 219c-49 0-90-41-90-91 0-50 41-91 90-91 49 0 90 41 90 91 0 50-41 91-90 91z m0-123c0-8 3-15 8-21-3 0-5 0-8 0-29 0-52 24-52 53 0 29 23 53 52 53 29 0 52-24 52-53 0-2 0-5 0-7-6 4-12 7-20 7-18 0-32-14-32-32z"/>
</symbol>
<symbol id="icon-ios-cloud-download" viewbox="0 0 512 512">
<path d="m248 435l-55-54-11 12 74 73 75-73-12-12-55 54 0-107-16 0z m151-271c0-1 0-2 0-3 0-64-52-115-116-115-46 0-86 27-105 65-8-4-17-6-27-6-29 0-54 23-58 51-36 12-61 44-61 83 0 49 40 89 90 89l126 0 0-135 16 0 0 135 134 0c45 0 82-37 82-82 0-45-35-81-81-82z"/>
</symbol>
<symbol id="icon-tick" viewbox="0 0 512 512">
<path d="m221 409c0 1 1 2 1 2 1 1 2 1 3 1 2 1 3 1 4 2 1 0 3 0 4 0 2 1 3 1 5 0 1 0 2 0 2 0 3-1 5-2 7-3 0 0 0 0 1-1 1 0 2-1 3-2 1-1 2-2 2-3 0 0 0 0 0 0l191-306c7-10 4-24-6-31-11-7-25-4-31 6l-177 283-97-95c-9-8-23-8-32 1-8 9-7 23 2 32l117 114c0 0 0 0 1 0z"/>
</symbol>
</svg>

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	
<!-- TOP BAR -->
<div class="header">
	<div class="container">
		<div class="row">

			<!-- LOGO -->
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
				<div class="logo">
					<img src="<?php echo get_template_directory_uri(); ?>/images/logo.svg" alt="">
				</div>
			</div>

			<!-- NAVIGATION -->
			<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
				<?php wp_nav_menu(array(
					'menu'            => 'primary',
					'container_class' => 'okayNav pull-right',
					'container_id'    => 'nav-main',
					'container'       => 'nav',
					'depth'           => 1
				)); ?>
			</div> <!-- / END OF NAVIGATION -->

		</div>
	</div>
</div>
<!-- / END OF TOP BAR -->