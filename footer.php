<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package TemplateOcean
 */

?>

	
<!-- FOOTER -->

<!-- FEATURED IMAGES -->
<section class="featured">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="text-center small-text">
					Our works featured on
				</div>
				<ul>
					<li><img src="<?php echo get_template_directory_uri(); ?>/images/featured/tripwirem.png" alt="TripwireMagazine"></li>
					<li><img src="<?php echo get_template_directory_uri(); ?>/images/featured/awww.png" alt="Awwwards"></li>
					<li><img src="<?php echo get_template_directory_uri(); ?>/images/featured/abduz.png" alt="Abduzeedo"></li>
					<li><img src="<?php echo get_template_directory_uri(); ?>/images/featured/ds.png" alt="Design Shack"></li>
				</ul>
			</div>
		</div>
	</div>
</section>
<section class="footer">
	<div class="container">
		<div class="row">
			<!-- RIGHT SIDE SUBSCRIPTION FORM -->
			<div class="col-lg-6 col-md-7 col-sm-12 pull-right">
				<div class="subcribption-area">
					<div class="subscription-box">
						Subscribe to our newsletter to get all latest updates about our product directly in your inbox. :)
						<form action="" class="mailchimp">
							<input type="email" name="email" id="email" placeholder="Enter your email address">
							<button class="subscribe standard-button">Subscribe</button>
						</form>
						<div class="follow">
							Follow Us On: <a href="">Facebook</a><a href="">Twitter</a><a href="">Dribbble</a>
						</div>
					</div>
				</div>
			</div>
			<!-- LEFT SIDE 2 NAVIGATION -->
			<div class="col-lg-6 col-md-5 col-sm-12 pull-left">
				<div class="row">
					<!-- FOOTER NAVIGATION 1 -->
					<div class="col-md-6 col-sm-6 col-xs-6">
						<ul>
							<li><a href="">About </a></li>
							<li><a href="">License</a></li>
							<li><a href="">Terms &amp; Condition</a></li>
							<li><a href="">Privacy Policy</a></li>
						</ul>
					</div>
					<!-- FOOTER NAVIGATION 2 -->
					<div class="col-md-6 col-sm-6 col-xs-6">
						<ul>
							<li><a href="">Work with Us</a></li>
							<li><a href="">Support</a></li>
							<li><a href="">Contact Us</a></li>
						</ul>
					</div>
				</div>
				<!-- FOOTER TEXT -->
				<div class="copyright">
					<div>
						≈
					</div>
					&#169; 2016 TemplateOcean
				</div>
			</div>
		</div>
	</div>
</section>

<?php wp_footer(); ?>

</body>
</html>
