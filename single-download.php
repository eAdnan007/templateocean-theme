<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package TemplateOcean
 */

get_header(); ?>
<?php if( have_posts() ) the_post(); ?>
<div class="single-template">
	<!-- NAVIGATION WILL APPEAR ON SCROLL WITH TEMPLATE NAME AND LINKS -->
	<div class="on-scroll-nav navbar-fixed-top hidden-sm hidden-xs">
		<div class="container">
			<div class="row">
				<div class="col-lg-7 col-md-7 col-xs-12">
					<!-- TEMPLATE TITLE ON NAV-->
					<div class="product-title">
						<?php the_title(); ?>
					</div>
				</div>
				<div class="col-lg-5 col-md-5 col-xs-12">
					<div class="navigation-right">
						<!-- LIVE PREVIEW LINK ON NAV -->
						<a href="<?php echo get_post_meta( $post->ID, '_live_preview_url', true ); ?>" class="normal-link">
							<svg class="icon-eye">
								<use xlink:href="#icon-eye"></use>
							</svg> Live Preview
						</a>
						<!-- TEMPLATE DOWNLOAD BUTTON ON NAV-->
						<a href="#download-template" class="standard-button smooth-scroll">Free Download</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- TEMPLATE TITLE, LIVE PREVIEW AND DOWNLOAD BUTTON -->
	<section class="intro">
		<div class="container">
			<div class="row">
				<div class="col-md-7">
					<!-- TEMPLATE TITLE -->
					<h1><?php the_title(); ?></h1>
					<div class="template-data">
						<!-- AUTHOR -->
						<div class="single-data">
							By: <?php the_author_link(); ?>
						</div>
						<!-- DOWNLOAD COUNTER -->
						<div class="single-data">
							<svg class="icon-download">
								<use xlink:href="#icon-ios-cloud-download"></use>
							</svg>
							<span class="download-count"><?php echo number_format( get_post_meta( $post->ID, '_edd_download_sales', true ), 0, '.', ',' ) ?></span>
						</div>
					</div>
				</div>
				<!-- DOWNLOAD AND LIVE PREVIEW LINKS -->
				<div class="col-md-5 text-right">
					<div class="buttons">
						<!-- LIVE PREVIEW -->
						<a href="<?php echo get_post_meta( $post->ID, '_live_preview_url', true ); ?>" class="normal-link">
							<svg class="icon-eye">
								<use xlink:href="#icon-eye"></use>
							</svg> Live Preview
						</a>
						<!-- DOWNLOAD BUTTON -->
						<a href="#download-template" class="btn-lg standard-button">Free Download Now</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="template-content">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<!-- FIRST IMAGE OF THE TEMPLATE -->
					<?php if( '' != trim( get_post_meta( $post->ID, '_cover_image', true ) ) ): ?>
					<img src="<?php echo get_post_meta( $post->ID, '_cover_image', true ); ?>" alt="<?php echo $post->post_title; ?>" class="img-responsive center-block">
					<?php endif ?>
					<!-- INTRO AND TEMPLATE DESCRITPTION  -->
					
					<?php the_content(); ?>
					<div class="row">
						<!-- MANUALLY ADD KEY FEATURES -->
						<h4>Key Features</h4>
						<ul class="feature-list">
							<?php $features = explode( "\r\n", get_post_meta( $post->ID, '_features', true ) ); ?>
							<?php foreach ($features as $feature): ?>
							<!-- SINGLE FEATURE -->
							<li>
								<svg class="icon-tick">
									<use xlink:href="#icon-tick"></use>
								</svg>
								<!-- THIS TEXT WILL BE ADDED FROM METABOX -->
								<h5><?php echo $feature; ?></h5>
							</li>
							<?php endforeach ?>
							
						</ul>
						<!-- TEMPLATE DOWNLOAD SECTION -->
						<div class="download-template" id="dowload-template">
							<div class="download-title">
								Free Download <span class="heavy"><?php echo get_post_meta( $post->ID, '_secondary_heading', true ); ?></span>
							</div>
							<?php if(!isset($_COOKIE['templateocean_download_with_email'])): ?>
							<form method="post">
								<input type="email" name="email" id="email" placeholder="Email Address">
								<button class="btn-lg standard-button">SEND ME THE TEMPLATE</button>
								<input type="hidden" name="edd_action" value="free_download_with_email">
								<?php wp_nonce_field( 'free_download', 'free_download_nonce' ); ?>
								<input type="hidden" name="free_download_id" value="<?php echo $post->ID; ?>">
							</form>
							<div class="small-text">
								You’ll be subscribed to our newsletter. We do not spam. :)
							</div>
							<?php else: ?>
								<div class="small-text">
									Already downloaded with email subscription. Replace this text with share and unlock code in single-download.php
								</div>
							<?php endif ?>
							<div class="template-details">
								<ul>
									<li>
										<h6>
										Released: </h6>
										<span class="small-text"><?php the_date(); ?></span>
									</li>
									<li>
										<h6>
										Last Updated: </h6>
										<span class="small-text"><?php the_modified_date(); ?></span>
									</li>
									<li>
										<h6>
										Version: </h6>
										<span class="small-text"><?php echo get_post_meta( $post->ID, '_version', true ); ?></span>
									</li>
									<li>
										<h6>
										Documentation: </h6>
										<span class="small-text">Yes ( <a href="<?php echo get_post_meta( $post->ID, '_documentation_url', true ); ?>">See Online</a> )</span>
									</li>
									<li>
										<h6>
										License: </h6>
										<span class="small-text"><a href="">Template License</a></span>
									</li>
								</ul>
							</div>
						</div>

						<!-- RELATED TEMPLATES -->
						<?php get_template_part( 'template-parts/related-posts' ); ?>
						<!-- COMMENTS -->
						<?php comments_template(); ?>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<?php
get_footer();
