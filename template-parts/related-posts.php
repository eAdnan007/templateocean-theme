<div class="related-templates">
	<h4>Similar Free Templates</h4>
	<div class="row">
		<div class="col-md-4 col-sm-4">
			<div class="related-template">
				<a href="">
					<img src="<?php echo get_template_directory_uri(); ?>/images/related-template.jpg" alt="">
					<h5>Fusion</h5>
				</a>
			</div>
		</div>
		<div class="col-md-4 col-sm-4">
			<div class="related-template">
				<a href="">
					<img src="<?php echo get_template_directory_uri(); ?>/images/related-template.jpg" alt="">
					<h5>Fusion</h5>
				</a>
			</div>
		</div>
		<div class="col-md-4 col-sm-4">
			<div class="related-template">
				<a href="">
					<img src="<?php echo get_template_directory_uri(); ?>/images/related-template.jpg" alt="">
					<h5>Fusion</h5>
				</a>
			</div>
		</div>
	</div>
</div>