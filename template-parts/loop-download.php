<!-- SINGLE TEMPLATE BOX -->
<div class="col-md-6">
	<div class="template-box">
		<div class="featured-image">
			<?php if( has_post_thumbnail() ): ?>
			<?php the_post_thumbnail ( 'loop-featured' ); ?>
			<?php else: ?>
			<img src="<?php echo get_template_directory_uri(); ?>/images/loop-featured-default.png" alt="No featured image">
			<?php endif; ?>
			<a href="<?php echo get_post_meta( $post->ID, '_live_preview_url', true ); ?>" class="live-preview">Live Preview</a>
		</div>
		<div class="template-link">
			<a href="<?php the_permalink(); ?>" class="pull-left name-link">
				<h3><?php echo get_post_meta( $post->ID, '_secondary_heading', true ); ?></h3>
				<span class="small-text"><?php echo get_post_meta( $post->ID, '_download_summary', true ); ?></span>
			</a>
			<a href="<?php the_permalink(); ?>" class="pull-right btn-small standard-button">Details &amp; Download</a>
		</div>
	</div>
</div>