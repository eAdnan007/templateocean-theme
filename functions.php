<?php
/**
 * TemplateOcean functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package TemplateOcean
 */

// remove the standard button that shows after the download's content
remove_action( 'edd_after_download_content', 'edd_append_purchase_link' );


if ( ! function_exists( 'templateocean_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function templateocean_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on TemplateOcean, use a find and replace
	 * to change 'templateocean' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'templateocean', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'templateocean' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'templateocean_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'templateocean_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function templateocean_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'templateocean_content_width', 640 );
}
add_action( 'after_setup_theme', 'templateocean_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function templateocean_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'templateocean' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'templateocean' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'templateocean_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function templateocean_scripts() {
	wp_enqueue_style( 'templateocean-style', get_stylesheet_uri() );

	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'templateocean', get_template_directory_uri() . '/js/scripts.js', array('jquery') );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'templateocean_scripts' );

/**
 * Enqueue scripts and styles on back end.
 */
function templateocean_backend_scripts() {
	wp_enqueue_media();

	// Registers and enqueues the required javascript.
	wp_register_script( 'tempaltecoean-backend', get_template_directory_uri() . '/js/backend.js', array( 'jquery' ) );
	wp_enqueue_script( 'tempaltecoean-backend' );
}
add_action( 'admin_enqueue_scripts', 'templateocean_backend_scripts' );

/**
 * Define metabox content for EDD download info.
 */
function download_info_metabox_content(){
	global $post;

	?>
	<table class="form-table">
		<tbody>
			<tr>
				<th scope="row"><label for="secondary-heading">Secondary Heading</label></th>
				<td><input type="text" name="download_info[secondary_heading]" class="regular-text" id="secondary-heading" value="<?php echo get_post_meta( $post->ID, '_secondary_heading', true ); ?>"></td>
			</tr>
			<tr>
				<th scope="row"><label for="download-summary">Summary</label></th>
				<td><input type="text" name="download_info[download_summary]" class="regular-text" id="download-summary" value="<?php echo get_post_meta( $post->ID, '_download_summary', true ); ?>"></td>
			</tr>
			<tr>
				<th scope="row"><label for="cover-image">Cover Image</label></th>
				<td>
					<input type="text" name="download_info[cover_image]" class="regular-text" id="cover-image" value="<?php echo get_post_meta( $post->ID, '_cover_image', true ); ?>">
					<input type="button" id="cover-image-button" class="button" value="Set Image" />
				</td>
			</tr>
			<tr>
				<th scope="row"><label for="live-preview-url">Live Preview URL</label></th>
				<td><input type="url" name="download_info[live_preview_url]" class="regular-text" id="live-preview-url" value="<?php echo get_post_meta( $post->ID, '_live_preview_url', true ); ?>"></td>
			</tr>
			<tr>
				<th scope="row"><label for="documentation-url">Documentation URL</label></th>
				<td><input type="url" name="download_info[documentation_url]" class="regular-text" id="documentation-url" value="<?php echo get_post_meta( $post->ID, '_documentation_url', true ); ?>"></td>
			</tr>
			<tr>
				<th scope="row"><label for="version">Version</label></th>
				<td><input type="text" name="download_info[version]" class="regular-text" id="version" value="<?php echo get_post_meta( $post->ID, '_version', true ); ?>"></td>
			</tr>
			<tr>
				<th scope="row"><label for="features">Features</label></th>
				<td><textarea name="download_info[features]" cols="45" rows="5" class="regular-text" id="features"><?php echo get_post_meta( $post->ID, '_features', true ); ?></textarea></td>
			</tr>
		</tbody>
	</table>
	<?php
	wp_nonce_field( 'download_info', 'download_info[nonce]' );
}


/**
 * Add metaboxes to EDD download posts.
 */
function templateocean_add_meta_boxes(){
	if( !class_exists( 'Easy_Digital_Downloads' ) ) return;

	add_meta_box ( 'templateocean-download-info', 'Download Info', 'download_info_metabox_content', 'download' );
}
add_action( 'add_meta_boxes', 'templateocean_add_meta_boxes' );


/**
 * Save download info.
 */
function templateocean_save_download_info( $post_id ){

	
	// If this is just a revision, don't send the email.
	if ( wp_is_post_revision( $post_id ) ) return;

	if( get_post_type( $post_id ) != 'download' ) return;


	// Extract the download info if available
	if( isset( $_POST['download_info'] ) )
		extract( $_POST['download_info'] );
	else
		return;


	// Check if provided nonce is valid
	if( !wp_verify_nonce( $nonce, 'download_info' ) ) return;

	update_post_meta( $post_id, '_secondary_heading', $secondary_heading );
	update_post_meta( $post_id, '_download_summary', $download_summary );
	update_post_meta( $post_id, '_cover_image', $cover_image );
	update_post_meta( $post_id, '_live_preview_url', $live_preview_url );
	update_post_meta( $post_id, '_documentation_url', $documentation_url );
	update_post_meta( $post_id, '_version', $version );
	update_post_meta( $post_id, '_features', $features );


}
add_action( 'save_post', 'templateocean_save_download_info' );

/**
 * Get the list of the free downloaders.
 */
function get_downloader_list_id(){
	$value = get_option( 'downloader_list_id' );
	echo '<input type="text" id="templateocean_downloader_list_id" name="downloader_list_id" value="' . $value . '" />';
}

/**
 * Initialization tasks.
 */
function templateocean_init(){


	// Add new size for featured image.
	add_image_size( 'loop-featured', 555, 350, true );

	add_rewrite_tag( '%sort%', 'popular|recent' );
	// Add rewrite rule for sorting
	add_rewrite_rule( "downloads/category/(.+?)/popular/?$", 'index.php?download_category=$matches[1]&sort=popular', 'top' );
	add_rewrite_rule( "downloads/category/(.+?)/popular/page/?([0-9]{1,})/?$", 'index.php?download_category=$matches[1]&sort=popular&paged=$matches[2]', 'top' );
}
add_action( 'init', 'templateocean_init' );

function templateocean_admin_init() {
	register_setting( 'general', 'downloader_list_id', 'esc_attr' ); 

	add_settings_field(
		'templateocean_downloader_list_id',
		'Downloader List ID',
		'get_downloader_list_id',
		'general',
		'default',
		array( 'label_for' => 'templateocean_downloader_list_id' )
	);
}
add_action( 'admin_init', 'templateocean_admin_init' );

/**
 * Filter and add comment support on EDD download post type.
 */
function templateocean_modify_edd_product_supports($supports) {
	$supports[] = 'comments';
	return $supports;	
}
add_filter('edd_download_supports', 'templateocean_modify_edd_product_supports');


/**
 * Send download mail and subscribe to mailing list.
 */
function templateocean_free_download_init() {

	if( ! isset( $_POST['free_download_nonce'] ) || ! wp_verify_nonce( $_POST['free_download_nonce'], 'free_download' ) )
		wp_die( 'Could not download file' );

	if ( ! isset( $_POST['email'] ) )
		wp_die( 'Email address was missing from submission.' );

	$email       = sanitize_email( trim( $_POST['email'] ) );
	if( ! is_email( $_POST['email'] ) || ! filter_var( $email, FILTER_VALIDATE_EMAIL ) )
		wp_die( 'Not a valid email address' );

	$download_id = isset( $_POST['free_download_id'] ) ? intval( $_POST['free_download_id'] ) : false;
	if ( empty( $download_id ) )
		wp_die( 'An internal error has occurred, please try again or contact support.' );

	$download = get_post( $download_id );

	// Bail if this isn't a valid download
	if( ! is_object( $download ) )
		wp_die( 'An internal error has occurred, please try again or contact support.' );

	if( 'download' != $download->post_type )
		wp_die( 'An internal error has occurred, please try again or contact support.' );

	// Bail if this isn't a published download (or the current user can't edit it)
	if( ! current_user_can( 'edit_post', $download->ID ) && $download->post_status != 'publish' )
		wp_die( 'An internal error has occurred, please try again or contact support.' );

	$user_info = array(
		'id'        => $user ? $user->ID : '-1',
		'email'     => $email,
		'discount'  => 'none'
	);

	$cart_details   = array();
	$download_files = edd_get_download_files( $download_id );

	if ( ! edd_is_free_download( $download_id ) )
		wp_die( 'An internal error has occurred, please try again or contact support' );

	// All test paseed, set a cookie to record that this visitor already downloaded with email.
	setcookie( 'templateocean_download_with_email', $download_id, time()+60*60*24*364 );


	$cart_details[0] = array(
		'name'      => get_the_title( $download_id ),
		'id'        => $download_id,
		'price'     => edd_format_amount( 0 ),
		'subtotal'  => edd_format_amount( 0 ),
		'quantity'  => 1,
		'tax'       => edd_format_amount( 0 )
	);

	$date = date( 'Y-m-d H:i:s', current_time( 'timestamp' ) );

	/**
	 * Gateway set to manual because manual + free lists as 'Free Purchase' in order details
	 */
	$purchase_data  = array(
		'price'         => edd_format_amount( 0 ),
		'tax'           => edd_format_amount( 0 ),
		'post_date'     => $date,
		'purchase_key'  => strtolower( md5( uniqid() ) ),
		'user_email'    => $email,
		'user_info'     => $user_info,
		'currency'      => edd_get_currency(),
		'downloads'     => array( $download_id ),
		'cart_details'  => $cart_details,
		'gateway'       => 'manual',
		'status'        => 'pending'
	);

	$payment_id = edd_insert_payment( $purchase_data );

	edd_update_payment_status( $payment_id, 'publish' );
	edd_insert_payment_note( $payment_id, 'Purchased through EDD Free Downloads' );
	edd_empty_cart();
	edd_set_purchase_session( $purchase_data );

	$payment_meta = edd_get_payment_meta( $payment_id );

	$redirect_url = edd_get_success_page_url();

	if(class_exists('SIB_Manager')){
		$instance = SIB_Manager::$instance;
		$instance->simple_signup( $email, [], get_option( 'downloader_list_id' ) );
	}

	wp_redirect( apply_filters( 'edd_free_downloads_redirect', $redirect_url, $payment_id, $purchase_data ) );
	edd_die();
}
add_action( 'edd_free_download_with_email', 'templateocean_free_download_init' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';
