<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package TemplateOcean
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php 

		$sort = get_query_var( 'sort' );
		$popular = 'popular' == $sort;
		if( $popular ){
			
			$q = array_merge( $wp_query->query, array(
				'meta_key'  => '_edd_download_sales',
				'order'     => 'DESC',
				'orderby'   => 'meta_value' ) );

			unset( $wp_query );
			$wp_query = new WP_Query( $q );

			$link = $_SERVER['REQUEST_URI'];
			$link = preg_replace("/(category\/[^\/]+\/)popular\/?/", "$1", $link);
		}
		else {

			$link = $_SERVER['REQUEST_URI'];
			$link = preg_replace("/(category\/[^\/]+)\/?/", "$1/popular/", $link);

		}
		?>
		<section class="intro">
			<div class="container">
				<div class="row">

					<div class="col-md-12">
						<?php
							the_archive_title( '<h1 class="page-title">', '</h1>' );
							the_archive_description( '<h6 class="taxonomy-description">', '</h6>' );
						?>
					</div>
				</div>
			</div>
		</section>

		<?php
		if ( have_posts() ) : ?>
			<section class="template-grid">
				<div class="container">
					<div class="row">
						<div class="col-md-12">

							<div class="sorting-link">
							<?php if( $popular ): ?>
								<a href="#" class="active">
							<?php else : ?>
								<a href="<?php echo $link; ?>" class="inactive">
							<?php endif; ?>

								Most Popular
							</a>
							

							<?php if( !$popular ): ?>
								<a href="#" class="active">
							<?php else : ?>
								<a href="<?php echo $link; ?>" class="inactive">
							<?php endif; ?>
								Recently Added
							</a>
							</div>
						</div>

						<?php 
						/* Start the Loop */
						while ( have_posts() ) : the_post();

							/*
							 * Include the Post-Format-specific template for the content.
							 * If you want to override this in a child theme, then include a file
							 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
							 */
							get_template_part( 'template-parts/loop', 'download' );

						endwhile;

						?>

					</div>
				</div>
			</section>


			<?php
			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
