<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package TemplateOcean
 */

get_header(); ?>

	<!-- TOP SECTION WITH HEADING -->
	<section class="intro">
		<div class="container">
			<div class="row">

				<div class="col-md-12">
					<!-- HEADING -->
					<h1>Free High Quality Website Templates</h1>

					<!-- DESCRIPTION -->
					<h6>Download free premium quality Responsive, HTML5, Bootstrap Templates</h6>

				</div>
			</div>
		</div>
	</section>

	<!-- TEMPLATE GRID FOR INDEX AND CATEGORY PAGES -->
	<section class="template-grid">
		<div class="container">

			<!-- RECENT TEMPLATES -->
			<div class="row">

				<!-- Heading -->
				<div class="col-md-12"><h2>Recent Templates</h2></div>

				<?php 
				/* Start the Loop */
				$latest_downloads = new WP_Query( array( 'post_type' => 'download' ) );
				while ( $latest_downloads->have_posts() ) : $latest_downloads->the_post();

					/*
					 * Include the Post-Format-specific template for the content.
					 */
					get_template_part( 'template-parts/loop', 'download' );

				endwhile;
				?>
			</div>
			<!-- /END OF RECENT TEMPLATES -->

			<!-- **************************************
			****** PAGINATION PLUGIN WILL BE HERE *****
			***************************************** -->
			<?php 
			/* Start the Loop */
			$popular_downloads = new WP_Query( array( 
				'post_type' => 'download',
				'meta_key'  => '_edd_download_sales',
				'order'     => 'DESC',
				'orderby'   => 'meta_value' ) );
			?>
			
			<?php if( $popular_downloads->have_posts() ): ?>
			<!-- POPULAR TEMPLATES -->
			<div class="row">
				<!-- HEADING -->
				<div class="col-md-12"><h2>Popular Templates</h2></div>

				<?php 
				while ( $popular_downloads->have_posts() ) : $popular_downloads->the_post();

					/*
					 * Include the Post-Format-specific template for the content.
					 */
					get_template_part( 'template-parts/loop', 'download' );

				endwhile;
				?>

			</div>
			<!-- END OF POPULAR TEMPLATES -->
			<?php endif ?>

		</div>

	</section> <!-- / END OF TEMPLATE GRID -->

<?php
get_footer();
